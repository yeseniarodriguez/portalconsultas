package com.atrums.portal.model;

import java.math.BigDecimal;
import java.util.Date;

public class Documento {
	
	public enum TipoDoc {FACTURA, NOTA_DE_CREDITO, RETENCION, GUIA_REMISION};
	String id;
	String tipo;
	String numero;
	Date fecha;
	String autorizacion;
	BigDecimal valor;
	String estado;
	String nc;
	
//	public void Documento() {
//		this.tipo = tipo;
//		this.numero = numero;
//		this.fecha = fecha;
//		this.autorizacion = autorizacion;
//		this.valor = valor;
//		this.estado = estado;
//	}
	
	public String getTipo() {
		return tipo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNc() {
		return nc;
	}
	public void setNc(String nc) {
		this.nc = nc;
	}

	

}
