package com.atrums.portal.services;

public interface IDocumento {

	public String searchFacturas(String user, String empresa);
	
	public String searchRetenciones(String user, String empresa);
	
	public String searchGuias(String user, String empresa);
}
