package com.atrums.portal.services;

public interface ITercero {
	public String searchTercero(String user, String empresa);
	
	public String searchPass(String bpartnerId, String pass);
	
	public String updatePass(String bpartnerId, String pass);
}
