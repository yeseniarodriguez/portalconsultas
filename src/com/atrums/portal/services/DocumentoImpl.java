package com.atrums.portal.services;

public class DocumentoImpl implements IDocumento {

	@Override
	public String searchFacturas(String user, String empresa) {
		String qryFacturas = " " +
				"select " +
				" i.em_co_nro_estab || '-' || i.em_co_punto_emision || '-' || documentno as numero, " +
				" i.dateinvoiced::date as fecha, " +
				" i.em_co_nro_aut_sri as autorizacion, " +
				" i.grandtotal as valor, " +
				" i.em_atecfe_menobserror_sri as estado," +
				" i.c_invoice_id as cid, " +
				" dt.isreversal as nc " +
				" from c_invoice i		    " +
				" join c_bpartner b on i.c_bpartner_id = b.c_bpartner_id " +
				" join c_doctype dt on i.c_doctypetarget_id = dt.c_doctype_id " +
				" where b.taxid = '" + user + "' " +
				" and i.em_atecfe_menobserror_sri like '%AUTORIZADO%' " +
				" and i.ad_client_id = '" + empresa + "' " +
				" order by 2 desc limit 100 ;";
		return qryFacturas;
	}

	@Override
	public String searchRetenciones(String user, String empresa) {
		String qryRetenciones = " " +
				"select " +
				" o.em_co_nro_estab || '-' || o.em_co_punto_emision || '-' || r.documentno as numero, " +
				" r.fecha_emision::date as fecha, " +
				" r.no_autorizacion as autorizacion, " +
				" r.total_retencion as valor, " +
				" r.em_atecfe_menobserror_sri as estado, " +
				" r.co_retencion_compra_id as cid, " +
				" 'N' as nc " +
				" from co_retencion_compra r " +
				"  join ad_org o on r.ad_org_id = o.ad_org_id " +
				"  join c_bpartner bp on r.c_bpartner_id = bp.c_bpartner_id " +
				" where bp.taxid = '" + user + "' " +
				" and r.em_atecfe_menobserror_sri like '%AUTORIZADO%' " +
				" and r.ad_client_id = '" + empresa + "' " +
				" order by 2 desc limit 100 ;";
		return qryRetenciones;
	}

	@Override
	public String searchGuias(String user, String empresa) {
		String qryGuias = " " +
				"select " +
				" o.em_co_nro_estab || '-' || o.em_co_punto_emision || '-' || i.documentno as numero, " +
				" i.movementdate::date as fecha, " +
				" i.em_atecfe_nro_aut_sri as autorizacion, " +
				" 0 as valor, " +
				" i.em_atecfe_menobserror_sri as estado," +
				" i.m_inout_id as cid, " +
				" 'N' as nc " +
				" from m_inout i		    " +
				"  join c_bpartner b on i.c_bpartner_id = b.c_bpartner_id " +
				"  join ad_org o on i.ad_org_id = o.ad_org_id " +
				" where b.taxid = '" + user + "' " +
				" and i.em_atecfe_menobserror_sri like '%AUTORIZADO%' " +
				" and i.ad_client_id = '" + empresa + "' " +
				" order by 2 desc limit 100 ;";
		return qryGuias;
	}

}
