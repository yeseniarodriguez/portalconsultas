package com.atrums.portal.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.atrums.portal.model.Empresa;

public class ListaEmpresas {

	public static List<Empresa> getListFromXML(String archivo) {
		
		List<Empresa> lsEmpresa = new ArrayList<Empresa>();
		
		try {
			
			File fXmlFile = new File(archivo);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("empresa");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					Empresa e = new Empresa();
					
					e.setId(eElement.getElementsByTagName("id").item(0).getTextContent());
					e.setNombre(eElement.getElementsByTagName("nombre").item(0).getTextContent());
					e.setDatasource(eElement.getElementsByTagName("datasource").item(0).getTextContent());
					
					lsEmpresa.add(e);
				}
			}
			
			return lsEmpresa;
			
	    } catch (Exception e) {
			e.printStackTrace();
			return null;
	    }
		
	}
	
}
