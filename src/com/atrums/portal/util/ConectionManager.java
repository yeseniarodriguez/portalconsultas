package com.atrums.portal.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.atrums.portal.servlet.Login;

public class ConectionManager {

	public static Connection abrirConexion() {
	
		
	try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env/");
		DataSource ds = (DataSource)envCtx.lookup(Login.DataSource);
			return ds.getConnection();
		}
		catch (Exception e ){
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	public static void cerrarConexion(Connection xcon) {
		try {
			xcon.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
