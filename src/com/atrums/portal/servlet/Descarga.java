package com.atrums.portal.servlet;

import java.io.ByteArrayInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperExportManager;
import com.atrums.portal.model.Documento;
import com.atrums.portal.util.ConectionManager;

/**
 * Servlet implementation class Descarga
 */
public class Descarga extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Descarga() {
        super();
    }
    
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req,resp);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String tipo = request.getParameter("Type");
		String idDoc = request.getParameter("DocID");
		String docType = request.getParameter("DocType");
		Login.DataSource = request.getParameter("Ds");
		
				
		if(tipo.equals("xml")) {
			descargaXml(idDoc, docType, response, Login.DataSource);
		}
		else if(tipo.equals("pdf")) {
			descargaPdf(idDoc, docType, response, Login.DataSource);
		}
		
		request.getRequestDispatcher("consulta.jsp");
	}
	
	public void descargaXml(String id, String dType, HttpServletResponse response, String DS) {

		Connection conn = ConectionManager.abrirConexion();
		
		String table = "c_invoice";
        if(dType.equals(Documento.TipoDoc.GUIA_REMISION.toString()))
        	table = "m_inout";
        if(dType.equals(Documento.TipoDoc.RETENCION.toString()))
        	table = "co_retencion_compra";
		
        try {
			Statement stmt = conn.createStatement();
			//consulta datos xml desde BDD
			
			String qryXml = "";
			
			if (table.equals("c_invoice")) {
			    qryXml = "select convert_from(documento_xml, 'UTF-8') as xml " +
					" from atecfe_c_invoice" + 
					"  where c_invoice_id = '" + id + "' ;";
			}
			/*else if (DS.toString().equals("jdbc/ELITETVErp") && table.equals("c_invoice")) {
				qryXml = "select convert_from(em_atecfe_documento_xml, 'UTF-8') as xml " +
						" from  " +table+
						"  where " + table + "_id = '" + id + "' ;";
			}
			else if (DS.toString().equals("jdbc/DIGITALErp") && table.equals("c_invoice")) {
				qryXml = "select convert_from(em_atecfe_documento_xml, 'UTF-8') as xml " +
						" from  " +table+
						"  where " + table + "_id = '" + id + "' ;";
			}
			else if (DS.toString().equals("jdbc/STARTVErp") && table.equals("c_invoice")) {
				qryXml = "select convert_from(em_atecfe_documento_xml, 'UTF-8') as xml " +
						" from  " +table+
						"  where " + table + "_id = '" + id + "' ;";
			}
			else if (DS.toString().equals("jdbc/DonTableroErp") && table.equals("c_invoice")) {
				qryXml = "select convert_from(em_atecfe_documento_xml, 'UTF-8') as xml " +
						" from  " +table+
						"  where " + table + "_id = '" + id + "' ;";
			}
			else if (DS.toString().equals("jdbc/ArtegelatoErp") && table.equals("c_invoice")) {
			    qryXml = "select convert_from(documento_xml, 'UTF-8') as xml " +
					" from atecfe_c_invoice" + 
					"  where c_invoice_id = '" + id + "' ;";
			}
			else if (DS.toString().equals("jdbc/DavilaErp") && table.equals("c_invoice")) {
			  qryXml = "select convert_from(documento_xml, 'UTF-8') as xml " +
					" from atecfe_c_invoice" + 
					"  where c_invoice_id = '" + id + "' ;";
			}*/
			
			else{
				qryXml = "select convert_from(em_atecfe_documento_xml, 'UTF-8') as xml " +
						" from " + table + 
						"  where " + table + "_id = '" + id + "' ;";
			}
			
			
			ResultSet rsXml = stmt.executeQuery(qryXml);
			if (rsXml.next()) {
				
				response.setContentType("application/octet-stream");
		        response.setHeader("Content-Disposition","attachment;filename=Documento.xml");
				
				ServletOutputStream out = response.getOutputStream();
				InputStream in = new ByteArrayInputStream(rsXml.getString(1).getBytes("UTF-8"));
				
				byte[] outputByte = new byte[4096];
				while(in.read(outputByte,0,4096)!= -1)
					out.write(outputByte,0,4096);
				in.close();
				out.flush();
				out.close();
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConectionManager.cerrarConexion(conn);
		}
			
		return;
	}
	
	public void descargaPdf(String id, String dType, HttpServletResponse response, String DS) {
		
		
		Connection conn = ConectionManager.abrirConexion();
		response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition","attachment;filename=Documento.pdf");
        
        String report = "";
        if (DS.toString().equals("jdbc/DavilaErp")){
        	report = "/reports/Davila/Rpt_Factura.jasper";
        }
        else if  (DS.toString().equals("jdbc/ELITETVErp")||DS.toString().equals("jdbc/DIGITALErp") ||DS.toString().equals("jdbc/STARTVErp"))
        {
        	report = "/reports/Starttv/Rpt_Factura.jasper";
        }
        else if  (DS.toString().equals("jdbc/DonTableroErp"))
        {
        	report = "/reports/Dontablero/Rpt_Factura.jasper";
        }
        else if  (DS.toString().equals("jdbc/ArtegelatoErp"))
        {
        	report = "/reports/Artegelato/Rpt_Factura.jasper";
        }
        else {
        	report = "/reports/Rpt_Factura.jasper";
        }
        	
         
        if(dType.equals(Documento.TipoDoc.GUIA_REMISION.toString()))
        {        
	        if (DS.toString().equals("jdbc/DavilaErp")){
	        	report = "/reports/Davila/Rpt_Guia.jasper";
	        }
	        else if  (DS.toString().equals("jdbc/ELITETVErp")||DS.toString().equals("jdbc/DIGITALErp") ||DS.toString().equals("jdbc/STARTVErp"))
	        {
	        	report = "/reports/Starttv/Rpt_Guia.jasper";
	        }
	        else if  (DS.toString().equals("jdbc/DonTableroErp"))
	        {
	        	report = "/reports/Dontablero/Rpt_Guia.jasper";
	        }
	        else if  (DS.toString().equals("jdbc/ArtegelatoErp"))
	        {
	        	report = "/reports/Artegelato/Rpt_Guia.jasper";
	        }
	        else{
	        	report = "/reports/Rpt_Guia.jasper";
	        }
        }
        
        if(dType.equals(Documento.TipoDoc.RETENCION.toString())){
        	if (DS.toString().equals("jdbc/DavilaErp")){
	        	report = "/reports/Davila/Rpt_Retenciones.jasper";
	        }
        	else if (DS.toString().equals("jdbc/ELITETVErp")||DS.toString().equals("jdbc/DIGITALErp") ||DS.toString().equals("jdbc/STARTVErp"))
        	{
        		report = "/reports/Starttv/Rpt_Retenciones.jasper";
	        }
        	else if  (DS.toString().equals("jdbc/DonTableroErp"))
	        {
	        	report = "/reports/Dontablero/Rpt_Retenciones.jasper";
	        }
        	else if  (DS.toString().equals("jdbc/ArtegelatoErp"))
	        {
	        	report = "/reports/Artegelato/Rpt_Retenciones.jasper";
	        }
        	else{
	        	report = "/reports/Rpt_Retencion.jasper";
	        }
        }
        
        if(dType.equals("NOTA DE CREDITO")){
        	if (DS.toString().equals("jdbc/DavilaErp")){
	        	report = "/reports/Davila/Rpt_NotaCredito.jasper";
	        }
        	else if  (DS.toString().equals("jdbc/ELITETVErp")||DS.toString().equals("jdbc/DIGITALErp") ||DS.toString().equals("jdbc/STARTVErp"))
        	{
	        	report = "/reports/Starttv/Rpt_NotaCredito.jasper";
	        }
        	else if  (DS.toString().equals("jdbc/DonTableroErp"))
	        {
	        	report = "/reports/Dontablero/Rpt_NotaCredito.jasper";
	        }
        	else if  (DS.toString().equals("jdbc/ArtegelatoErp"))
	        {
	        	report = "/reports/Artegelato/Rpt_NotaCredito.jasper";
	        }
        	else{
	        	report = "/reports/Rpt_NotaCredito.jasper";
	        }
        }
        
        String path = getServletContext().getRealPath(report);
        String url = getServletContext().getRealPath("");
        
		try {
			
			HashMap<String,Object> parameters = new HashMap<String,Object>();
			parameters.put("DOCUMENT_ID", id);
			parameters.put("BASE_DESIGN", url);
			parameters.put("DATASOURCE", Login.DataSource);
			
			JasperPrint jasPrint = JasperFillManager.fillReport(path, parameters, conn);
			
			ServletOutputStream sos = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasPrint, sos);
			
            sos.close();
            
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConectionManager.cerrarConexion(conn);
		}
        
		return;
	}
	
	
}
