package com.atrums.portal.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atrums.portal.model.Cliente;
import com.atrums.portal.model.Documento;
import com.atrums.portal.services.DocumentoImpl;
import com.atrums.portal.services.TerceroImpl;
import com.atrums.portal.util.ConectionManager;

/**
 * Servlet implementation class ListarDocumentos
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String DataSource;
	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
    }

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req,resp);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// inicializar conexion y listas
		String strOpcion = request.getParameter("empresa");
		String[] parts = strOpcion.split("\\*");
		String strDataSource = parts[0];
		String strEmpresa = parts[1];
		String strNombreEmpresa = parts[2];
		
		boolean correcto = true;
		DataSource = strDataSource;
		
		
		Connection conn = ConectionManager.abrirConexion();
		
		List<Cliente> lsClientes = new ArrayList<Cliente>();
		List<Documento> lsFacturas = new ArrayList<Documento>();
		List<Documento> lsGuias = new ArrayList<Documento>();
		List<Documento> lsRetenciones = new ArrayList<Documento>();
		
		try {
			Statement stmtTer = conn.createStatement();
			Statement stmtFac = conn.createStatement();
			Statement stmtRet = conn.createStatement();
			Statement stmtGuia = conn.createStatement();
			
			//consulta datos tercero
			TerceroImpl ti = new TerceroImpl();
		
			ResultSet rsTercero = stmtTer.executeQuery(ti.searchTercero(request.getParameter("user"), strEmpresa));
			
			if (!rsTercero.isBeforeFirst() ) {    
			    correcto = false;
			} 
			
			while(rsTercero.next()) {
				Cliente cliente = new Cliente();
				cliente.setNombre(rsTercero.getString("name"));
				cliente.setClave(rsTercero.getString("pass"));
				cliente.setIdentificador(request.getParameter("user"));
				cliente.setId(rsTercero.getString("c_bpartner_id"));
//				if(rsTercero.getString("pass") == null || rsTercero.getString("pass").equals("") || rsTercero.getString("pass").equals(null))
//					establecerPass(rsTercero.getString("c_bpartner_id"), request.getParameter("user"));
//				else
					if(!validarPass(rsTercero.getString("c_bpartner_id"), request.getParameter("password")))
						correcto = false;
				lsClientes.add(cliente);
			}
			
			//consulta datos facturas
			DocumentoImpl di = new DocumentoImpl();
			ResultSet rsFacturas = stmtFac.executeQuery(di.searchFacturas(request.getParameter("user"), strEmpresa));
			
			//consulta datos retenciones
			ResultSet rsRetenciones = stmtRet.executeQuery(di.searchRetenciones(request.getParameter("user"), strEmpresa));
			
			//consulta datos guias
			ResultSet rsGuias = stmtGuia.executeQuery(di.searchGuias(request.getParameter("user"), strEmpresa));
			
			if (!rsFacturas.isBeforeFirst() && rsRetenciones.isBeforeFirst() && rsGuias.isBeforeFirst()) {    
			    correcto = false;
			} 
			
			while(rsFacturas.next()) {
				Documento fact = new Documento();
				fact.setId(rsFacturas.getString("cid"));
				if (rsFacturas.getString("nc").equals("Y")) 
					fact.setTipo("NOTA DE CREDITO"); 
				else
					fact.setTipo(Documento.TipoDoc.FACTURA.toString());
				fact.setNumero(rsFacturas.getString("numero"));
				fact.setFecha(rsFacturas.getDate("fecha"));
				fact.setAutorizacion(rsFacturas.getString("autorizacion"));
				fact.setValor(rsFacturas.getBigDecimal("valor"));
				fact.setEstado(rsFacturas.getString("estado"));
				lsFacturas.add(fact);
			}
			
			while(rsRetenciones.next()) {
				Documento ret = new Documento();
				ret.setId(rsRetenciones.getString("cid"));
				ret.setTipo(Documento.TipoDoc.RETENCION.toString());
				ret.setNumero(rsRetenciones.getString("numero"));
				ret.setFecha(rsRetenciones.getDate("fecha"));
				ret.setAutorizacion(rsRetenciones.getString("autorizacion"));
				ret.setValor(rsRetenciones.getBigDecimal("valor").setScale(2));
				ret.setEstado(rsRetenciones.getString("estado"));
				lsRetenciones.add(ret);
			}
			
			while(rsGuias.next()) {
				Documento guia = new Documento();
				guia.setId(rsGuias.getString("cid"));
				guia.setTipo(Documento.TipoDoc.GUIA_REMISION.toString());
				guia.setNumero(rsGuias.getString("numero"));
				guia.setFecha(rsGuias.getDate("fecha"));
				guia.setAutorizacion(rsGuias.getString("autorizacion"));
				guia.setValor(rsGuias.getBigDecimal("valor"));
				guia.setEstado(rsGuias.getString("estado"));
				lsGuias.add(guia);
			}
			
			//enviar parametros para JSP
			if(correcto) {
				request.setAttribute("listFacts", lsFacturas);
				request.setAttribute("listGuias", lsGuias);
				request.setAttribute("listRets", lsRetenciones);
				request.setAttribute("listCliente", lsClientes);
				request.setAttribute("DS", strDataSource);
				request.setAttribute("EmpresaName", strNombreEmpresa);
			}
			
			//cerrar uso de conexion
			stmtTer.close();
			stmtFac.close();
			stmtRet.close();
			stmtGuia.close();
			
			// enviar a pagina de resultados consulta.jsp
			if(correcto)
				request.getRequestDispatcher("consulta.jsp").forward(request, response);
			else {
				request.setAttribute("mensaje", "El usuario o contraseņa son incorrectos, por favor intente nuevamente.");
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConectionManager.cerrarConexion(conn);
		}
		
	}
	
	public void establecerPass(String bpartnerId, String pass) {
		Connection conn = ConectionManager.abrirConexion();
		try {
			Statement stmtPass = conn.createStatement();
			
			TerceroImpl tPass = new TerceroImpl();
			stmtPass.execute(tPass.updatePass(bpartnerId, pass));
			
			stmtPass.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConectionManager.cerrarConexion(conn);
		}
	}
	
	public boolean validarPass(String bpartnerId, String pass) {
		Connection conn = ConectionManager.abrirConexion();
		try {
			Statement stmtPass = conn.createStatement();
			
			TerceroImpl tPass = new TerceroImpl();
			ResultSet rsPass = stmtPass.executeQuery(tPass.searchPass(bpartnerId, pass));
			
			if (!rsPass.isBeforeFirst() ) {  
				stmtPass.close();
			    return false;
			} 
				stmtPass.close();
				
				
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConectionManager.cerrarConexion(conn);
		}
		return true;
	}

}
