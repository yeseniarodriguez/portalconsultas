<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List"%>

<%@ page import="com.atrums.portal.util.ListaEmpresas" %>
<%@ page import="com.atrums.portal.model.Empresa" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<script language="javascript">  
function validarLogin(){  
	 var emp = document.frmLogin.empresa.value;  
	 var user = document.frmLogin.user.value; 
	 var pass = document.frmLogin.password.value; 
	   if (emp=="XX*00") {  
	     alert("Seleccione una empresa");  
	     return false;  
	   } 
	   else if (user == "9999999999999") {
		 alert("Ingrese su usuario");  
	     return false;
	   }
	   else if (user==null || user=="") {  
	     alert("Ingrese su usuario");  
	     return false;  
	   }
	   else if (pass==null || pass=="") {  
	     alert("Ingrese su contrase�a");  
	     return false;  
	   }
	}
	
function mostrarMensaje(){ 
   var s = "<%=request.getAttribute("mensaje")%>";
   if (s=="El usuario o contrase�a son incorrectos, por favor intente nuevamente.") {
   	 alert(s);
   }
   return false;  
}

</script> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Portal de Facturacion Atrums-IT</title>
	<link rel="stylesheet" href="css/style.css">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="script/menu.js"></script>
</head>

<body id="contenedor" onload="mostrarMensaje()">
	<header>
	    <div class="logotipo">
		    <a id="logo-cabecera" href="http://atrums.com">
		    	<img src="images/atrums-logo.png" id="logotipo-imagen" />
		    </a> 
	    </div>
	    <div class="menu-principal">
			<a href="#" class="btn-menu"><img src="images/barra-menu.png" /><p>MENU</p></a>
		</div>
		<!-- Navegacion -->
	    <nav>
	        <ul>
	        	<li><a href="http://atrums.com/contacto/" target="_blank">CONTACTO</a></li>
	        	<li><a href="login.jsp" target="_blank">INICIO</a></li>
	        </ul>
	    </nav>
	    <!-- Fin Navegacion -->
	</header>

<%
// Linux
String archivo = getServletContext().getRealPath("")+"/WEB-INF/empresas.properties";
// Windows
//String archivo = getServletContext().getRealPath("")+"\\WEB-INF\\empresas.properties";
List<Empresa> lstEmpresas = ListaEmpresas.getListFromXML(archivo); %>
<section>
	<div class="disenio_formulario" >
		<div class="cuadro-formulario">
			<form name="frmLogin" method="post" action="documentos" onsubmit="return validarLogin()">
				
				<label for="texto_iniciar" class="texto_iniciar">Iniciar sesi�n</label>
		
				<div class="bloque-formulario">
					
					<div class="cuadro-texto" >
						<label for="empresa" class="label">Empresa</label>
						<select name="empresa" class="select">
							<option value="XX*00" id="empresa" class="option">Seleccione...</option>
			      			<% for(Empresa e : lstEmpresas) { %>
			        			<option value="<%= e.getDatasource() %>*<%= e.getId() %>*<%= e.getNombre() %>" id="empresa" class="option"><%= e.getNombre() %></option>
			      			<% }; %>
					  	</select>
				  	</div>
				  	
					<div class="cuadro-texto">
						<label for="user" class="label">Usuario</label>
						<input id="user" name="user" type="text" class="input">
					</div>
				
					<div class="cuadro-texto">
						<label for="pass" class="label">Contrase�a</label>
						<input id="pass" name="password" type="password" class="input" data-type="password">
					</div>
					
					<div class="cuadro-texto">
						<input id="check" type="checkbox" class="check" checked>
						<label for="check"><span class="icon"></span> Mant�nme conectado</label>
					</div>
					
					<div class="cuadro-texto">
						<input type="submit" class="button" value="iniciar sesi�n" >
					</div>
				</div>
			</form>
		</div>
	</div>
</section>		
	<!-- FOOTER -->
	
	<footer id="pie-pagina">
		<div> � Copyright@2017 ATRUMS - Todos los Derechos reservados
	    </div>
	</footer>
	
	<!-- FINFOOTER -->
</body>
</html>