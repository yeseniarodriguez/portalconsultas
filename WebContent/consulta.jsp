<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List" %>

<%@ page import="com.atrums.portal.model.Cliente" %>
<%@ page import="com.atrums.portal.model.Documento" %>
<%@ page import="com.atrums.portal.servlet.Login" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<script language="javascript">
function cambiarPestanna(pestannas,pestanna) {
    
    // Obtiene los elementos con los identificadores pasados.
    pestanna = document.getElementById(pestanna.id);
    listaPestannas = document.getElementById(pestannas.id);
    
    // Obtiene las divisiones que tienen el contenido de las pesta�as.
    cpestanna = document.getElementById('c'+pestanna.id);
    listacPestannas = document.getElementById('contenido'+pestannas.id);
    
    i=0;
    // Recorre la lista ocultando todas las pesta�as y restaurando el fondo 
    // y el padding de las pesta�as.
    while (typeof listacPestannas.getElementsByTagName('div')[i] != 'undefined'){
        $(document).ready(function(){
            $(listacPestannas.getElementsByTagName('div')[i]).css('display','none');
            $(listaPestannas.getElementsByTagName('li')[i]).css('background','');
            $(listaPestannas.getElementsByTagName('li')[i]).css('padding-bottom','');
        });
        i += 1;
    }
 
    $(document).ready(function(){
        // Muestra el contenido de la pesta�a pasada como parametro a la funcion,
        // cambia el color de la pesta�a y aumenta el padding para que tape el  
        // borde superior del contenido que esta juesto debajo y se vea de este 
        // modo que esta seleccionada.
        $(cpestanna).css('display','');
        $(pestanna).css('background','dimgray');
        $(pestanna).css('padding-bottom','2px'); 
    });
 
}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
	<title>Facturacion electronica Atrums-IT</title>
	<link rel="stylesheet" href="css/style.css">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="script/menu.js"></script>
</head>

<body onload="javascript:cambiarPestanna(pestanas,pestana1);">

	<header>
	    <div class="logotipo">
		    <a id="logo-cabecera" href="http://atrums.com">
		    	<img src="images/atrums-logo.png" id="logotipo-imagen" />
		    </a> 
	    </div>
	    <div class="menu-principal">
			<a href="#" class="btn-menu"><img src="images/barra-menu.png" /><p>MENU</p></a>
		</div>
		<!-- Navegacion -->
	    <nav>
	        <ul>
	            <li><a href="http://atrums.com/contacto/">CONTACTO</a></li>
	            <li><a href="login.jsp">CERRAR SESI�N</a></li>
	        </ul>
	    </nav>
	    <!-- Fin Navegacion -->
	</header>

<% List<Documento> lsFacturas = (List<Documento>) request.getAttribute("listFacts");
   List<Documento> lsGuias = (List<Documento>) request.getAttribute("listGuias");
   List<Documento> lsRets = (List<Documento>) request.getAttribute("listRets");
   List<Cliente> lsClientes = (List) request.getAttribute("listCliente"); %>
<section>
	<div id="content-interna">
		<div id="content-detalle">    
	    	<div>
				<label id="lbl-texto">EMPRESA: </label> <label id="lbl-content"> <%= request.getAttribute("EmpresaName") %> </label>
			</div>
			<div>
				<label id="lbl-texto">CI / RUC: </label> <label id="lbl-content"><%= lsClientes.get(0).getIdentificador() %> </label>
			</div>
			<div>
				<label id="lbl-texto">RAZON SOCIAL / NOMBRE COMERCIAL: </label> <label id="lbl-content"> <%= lsClientes.get(0).getNombre() %> </label>
			</div>
	    </div>
	    <div id="content-tabla" >   
			<form method="post" action="descarga" >
				<div id="pestanas">
		            <ul id=lista>
		                <li id="pestana1"><a href='javascript:cambiarPestanna(pestanas,pestana1);'>FACTURAS</a></li>
		                <li id="pestana2"><a href='javascript:cambiarPestanna(pestanas,pestana2);'>GUIAS REMISI�N</a></li>
		                <li id="pestana3"><a href='javascript:cambiarPestanna(pestanas,pestana3);'>RETENCIONES</a></li>
		            </ul>
		        </div>
		        <div id="contenidopestanas">
		            <div id="cpestana1">
		                <table >
				            <tr>
				            	<th>Tipo Comprobante</th>
				                <th>Numero Documento</th>
				                <th>Fecha Emision</th>
				                <th>Numero Autorizacion</th>
				                <th>Monto</th>
				                <th>Estado</th>
				                <th>XML</th>
				                <th>PDF</th>
				            </tr>
				            <% for(Documento d : lsFacturas) { %>
				                <tr>
				                	<td><% out.print(d.getTipo()); %></td>
				                    <td><% out.print(d.getNumero()); %></td>
				                    <td><% out.print(d.getFecha()); %></td>
				                    <td><% out.print(d.getAutorizacion()); %></td>
				                    <td><% out.print(d.getValor()); %></td>
				                    <td><% out.print(d.getEstado()); %></td>
				                    <td align="center">
				                    	<a href="/portal/descarga?DocID=<%=d.getId() %>&DocType=<%=d.getTipo() %>&Ds=<%=request.getAttribute("DS").toString() %>&Type=xml">
				                    	<img src="images/xml_icon.png" width="20" height="20" alt="Descargar XML"></td>
				                    <td align="center">
				                    	<a href="/portal/descarga?DocID=<%=d.getId() %>&DocType=<%=d.getTipo() %>&Ds=<%=request.getAttribute("DS").toString() %>&Type=pdf">
				                    	<img src="images/pdf_icon.png" width="20" height="20" alt="Descargar PDF"></td>
				                </tr>
				            <% }; %>
				        </table>
		            </div>
		            <div id="cpestana2">
		                <table >
				            <tr>
				            	<th>Tipo Comprobante</th>
				                <th>Numero Documento</th>
				                <th>Fecha Emision</th>
				                <th>Numero Autorizacion</th>
				                <th>Estado</th>
				                <th>XML</th>
				                <th>PDF</th>
				            </tr>
				            <% for(Documento d : lsGuias) { %>
				                <tr>
				                	<td><% out.print(d.getTipo()); %></td>
				                    <td><% out.print(d.getNumero()); %></td>
				                    <td><% out.print(d.getFecha()); %></td>
				                    <td><% out.print(d.getAutorizacion()); %></td>
				                    <td><% out.print(d.getEstado()); %></td>
				                    <td align="center">
				                    	<a href="/portal/descarga?DocID=<%=d.getId() %>&DocType=<%=d.getTipo() %>&Ds=<%=request.getAttribute("DS").toString() %>&Type=xml">
				                    	<img src="images/xml_icon.png" width="20" height="20" alt="Descargar XML"></td>
				                    <td align="center">
				                    	<a href="/portal/descarga?DocID=<%=d.getId() %>&DocType=<%=d.getTipo() %>&Ds=<%=request.getAttribute("DS").toString() %>&Type=pdf">
				                    	<img src="images/pdf_icon.png" width="20" height="20" alt="Descargar PDF"></td>
				                </tr>
				            <% }; %>
				        </table>
		            </div>
		            <div id="cpestana3">
		                <table >
				            <tr>
				            	<th>Tipo Comprobante</th>
				                <th>Numero Documento</th>
				                <th>Fecha Emision</th>
				                <th>Numero Autorizacion</th>
				                <th>Monto</th>
				                <th>Estado</th>
				                <th>XML</th>
				                <th>PDF</th>
				            </tr>
				            <% for(Documento d : lsRets) { %>
				                <tr>
				                	<td><% out.print(d.getTipo()); %></td>
				                    <td><% out.print(d.getNumero()); %></td>
				                    <td><% out.print(d.getFecha()); %></td>
				                    <td><% out.print(d.getAutorizacion()); %></td>
				                    <td><% out.print(d.getValor()); %></td>
				                    <td><% out.print(d.getEstado()); %></td>
				                    <td align="center">
				                    	<a href="/portal/descarga?DocID=<%=d.getId() %>&DocType=<%=d.getTipo() %>&Ds=<%=request.getAttribute("DS").toString() %>&Type=xml">
				                    	<img src="images/xml_icon.png" width="20" height="20" alt="Descargar XML"></td>
				                    <td align="center">
				                    	<a href="/portal/descarga?DocID=<%=d.getId() %>&DocType=<%=d.getTipo() %>&Ds=<%=request.getAttribute("DS").toString() %>&Type=pdf">
				                    	<img src="images/pdf_icon.png" width="20" height="20" alt="Descargar PDF"></td>
				                </tr>
				            <% }; %>
				        </table>
		            </div>
		    	</div>
		    </form>
		</div>
    </div>
</section>
	<!-- FOOTER -->
	
	<footer id="pie-pagina">
		<div> � Copyright@2017 ATRUMS - Todos los Derechos reservados
	    </div>
	</footer>

<!-- FINFOOTER -->
</body>
</html>